using UnityEngine;

public class Invader : MonoBehaviour
{
    public Sprite[] animationSprites;
    //time for 1 sprite
    public float animationTime = 1.0f;

    private SpriteRenderer _spriteRenderer;
    private int _animationFrame;

    public System.Action killed;

    // initialize any variables or game state before the game starts
    private void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    //Awake is called first and, unlike Start, will be called even if the script component is disabled
    private void Start()
    {
        InvokeRepeating(nameof(AnimationSprite),this.animationTime, this.animationTime);
    }

    private void AnimationSprite()
    {
        _animationFrame++;

        if(_animationFrame >= this.animationSprites.Length)
        {
            _animationFrame = 0;
        }

        _spriteRenderer.sprite = this.animationSprites[_animationFrame];
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        //check if missile hit invader
        if(other.gameObject.layer == LayerMask.NameToLayer("Missile"))
        {
            this.killed.Invoke();
            this.gameObject.SetActive(false);
        }
    }
}
