using UnityEngine;
using UnityEngine.SceneManagement;

public class Orcs : MonoBehaviour
{
    //grid of orcs, we can change in unity
    public int rows = 5;
    public int columns = 5;

    public Invader[] prefabs;

    //for movement
    private Vector3 _direction = Vector2.left;
    private Vector3 _verticalDirection = Vector2.down;

    //speed
    //x
    public AnimationCurve horisontalSpeed;
    //y
    public float verticalSpeed = 10.0f;

    //prefab for enemy missiles
    public Projectile missilePrefab;
    //time for arcs to attack
    public float orc_missileAttackRate = 1.0f;

    //how many is alive
    public int amountAlive => this.totalOrcs - this.AmountKilled;

    //amount that killed
    public int AmountKilled {get; private set;}
    //total amount
    public int totalOrcs => this.rows * this.columns;
    //in percentage
    public float percentKilled => (float)this.AmountKilled / (float)this.totalOrcs;

    private void Awake()
    {
        for(int row = 0; row < this.rows; row++)
        {
            //height and width of our grid
            float width = 5.0f * (this.columns - 1);
            float height = 20.0f * (this.rows - 1);

            //has half of size of the grid
            Vector3 centering = new Vector3(-width/2, -height/2);

            //position x, y, z for a unit
            //position for a row of units
            Vector3 rowPosition = new Vector3(centering.x, centering.y +( row * 25.0f), 0.0f);

            for(int col = 0; col< this.columns; col++)
            {
                Invader orc = Instantiate(this.prefabs[row], this.transform);

                orc.killed += OrcKilled;

                //change location of orc
                Vector3 position = rowPosition;

                //now position on osi OX
                position.x += col * 25.0f;
                
                orc.transform.localPosition = position;

            }
        }
    }

    // Start is called before the first frame update
    private void Start()
    {
        InvokeRepeating(nameof(OrcMissileAttack), this.orc_missileAttackRate, this.orc_missileAttackRate);
    }

    //update state for every frame game is running
    private void Update()
    {
        //moving them
        //x, will be faster if many killed
        this.transform.position += _direction * this.horisontalSpeed.Evaluate(this.percentKilled) * Time.deltaTime;
        //y
        this.transform.position += _verticalDirection * this.verticalSpeed * Time.deltaTime;

        //get endeg of our playboard
        //my edge is 70 -70 but it dont work
        Vector3 upEdge = Camera.main.ViewportToWorldPoint(Vector2.up);
        Vector3 downEdge = Camera.main.ViewportToWorldPoint(Vector2.down);


        //print in unity console
        //print(downEdge);



        //cheaking if they hit borders, need to cheeck all childs
        foreach (Transform orc in this.transform)
        {
            //cheeck if it alive
            if(!orc.gameObject.activeInHierarchy)
            {
                continue;
            }

            if(_verticalDirection == Vector3.up && orc.position.y >= (70 - 1.0f))
            {
                AdvanceMove();
            }
            else if(_verticalDirection == Vector3.down && orc.position.y <= (-70 + 1.0f))
            {
                AdvanceMove();
            }
        }
    }

    //change direction
    private void AdvanceMove()
    {
        _verticalDirection.y *= -1.0f;

        //move next row, dont need, because we have move already
        //Vector3 position = this.transform.position;
        //position.x -= 1.0f;
        //this.transform.position = position;
    }

    //when delegate invoke, that orc is killed
    private void OrcKilled()
    {
        this.AmountKilled++;

        //win condition
        //here will be more improvements

        /*
        if(this.AmountKilled >= this.totalOrcs)
        {
            //reload entire scene
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
        */
    }
    //orc attack us
    private void OrcMissileAttack()
    {
        //each orc has chance to shoot missile
        foreach (Transform orc in this.transform)
        {
            //cheeck if it alive
            if(!orc.gameObject.activeInHierarchy)
            {
                continue;
            }
            //fewer orcs, then more chances to shoot enemy missile
            if(Random.value < (1.0f /(float)this.amountAlive))
            {
                Instantiate(this.missilePrefab, orc.position, Quaternion.identity);
                //as soon as we have one, stop shooting enemy missiles
                break;
            }
        }
    }
}
