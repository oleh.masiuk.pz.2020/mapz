using UnityEngine;

public class Projectile : MonoBehaviour
{
    //speed is different for enemy
    public Vector3 direction;

    public float speed;

    //event that missile is destroyed
    public System.Action destroyed;

    public void Update()
    {
        this.transform.position += this.direction * this.speed * Time.deltaTime;
    }

    //check for collision
    private void OnTriggerEnter2D(Collider2D other)
    {
        if(this.destroyed != null)
        {
            this.destroyed.Invoke();
        }
        
        Destroy(this.gameObject);
    }
}
