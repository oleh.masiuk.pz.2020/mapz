﻿using System;
using System.Diagnostics;
using System.Threading;

namespace SigmaKR
{
    public class MyStatClas
    {
        static int stat_instance;
        static MyStatClas()
        {
            stat_instance = 0;
        }
        public MyStatClas(dynamic obj1, dynamic obj2)
        {
            Console.WriteLine("The type of first is {0}\n the type of second is {1}",
                obj1.GetType(),obj2.GetType());
        }
    }
    class Parrent
    {
        public Parrent(string mes)
        {
            Console.WriteLine(mes);
        }
        public virtual void DoSomething()
        {
            Console.WriteLine("Hello Parent");
        }
    }
    class Child:Parrent
    {
        int age;
        public Child(string child_mes, string par_mes):base(par_mes)
        {
            Console.WriteLine(child_mes);
            this.age = 10;
        }
        public override void DoSomething()
        {
            base.DoSomething();
            Console.WriteLine(this.age);
        }
    }
    
    class MyTestClass
    {
        public int myInt { get; set; }
        public MyTestClass(int num)
        {
            this.myInt = num;
        }
        public static implicit operator int(MyTestClass obj) => obj.myInt;
        public static explicit operator MyTestClass(int num) => new MyTestClass(num);

        public override string ToString()
        {
            string res = "Num int is " + myInt.ToString();
            return res;
        }
        public override int GetHashCode()
        {
            return myInt * 100 - 9;
        }
        public override bool Equals(object obj)
        {
            bool res = false;
            if(obj is MyTestClass)
            {
                MyTestClass test_obj = obj as MyTestClass;
                if(test_obj.myInt == this.myInt)
                {
                    res = true;
                }
            }
            return res;
        }

    }

    //частина про інтерфейси
    interface FirstI
    {
    }
    interface SecondI
    {

    }
    interface ThirdI
    {

    }
    interface ForthI
    {

    }
    interface FifthI
    {

    }
    interface SixthI
    {

    }


    class TestInterface:FirstI,SecondI,ThirdI,ForthI,FifthI,SixthI
    {
        public TestInterface()
        {

        }

        ~TestInterface()
        {
            int x = 10 - 10;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch timer = new Stopwatch();
            timer.Start();
            for(int i =0; i<1000000; i++)
            {
                TestInterface test = new TestInterface();
            }

            timer.Stop();


            TimeSpan timeTaken = timer.Elapsed;
            string foo = "Time taken: " + timeTaken.ToString(@"m\:ss\.fff");
            Console.WriteLine(foo);
            //Thread.Sleep(2000);
        }
    }
}
