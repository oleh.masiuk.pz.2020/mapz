﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_lab2
{
    public class Storage
    {
        public string StorageLocation { get; set; }

        public List<Product> Products { get; private set; }

        public Storage()
        {
            Products = new List<Product>();
        }
        public Storage(string location, List<Product> products)
        {
            StorageLocation = location;
            Products = products;

        }

        // індексатор
        public Product this[int index]
        {
            get => Products[index];
            set => Products[index] = value;
        }

        public override string ToString()
        {
            string res = String.Format($"Storage locaion:{StorageLocation}\nProducts:\n");

            Products.ForEach(product =>res+= product.ToString());

            return res+"\n";
        }
    }
}
