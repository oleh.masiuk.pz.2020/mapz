﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_lab2
{
    class ModelTest
    {
        public static List<Storage> GetStorages()
        {
            return new List<Storage>()
            {
                new Storage("Lviv", GetProducts()),
                new Storage("London", GetProducts()),
                new Storage("New York", GetProducts()),
                new Storage("Kyiv", GetProducts()),
                new Storage("Yavoriv", GetProducts())
            };
        }

        public static List<Product> GetProducts()
        {

            return new List<Product>()
            {
                new Product{Name = "Apple Jam", Price=3.99 },
                new Product{Name = "Rice", Price=5.65 },
                new Product{Name = "Meatball", Price=10.34 },
                new Product{Name = "Cheesse", Price=9.43 },
                new Product{Name = "Milk", Price=5.99 },
                new Product{Name = "Nut", Price=1.89 },
                new Product{Name = "Jam", Price=5.99 }
            };
        }
    }
}
