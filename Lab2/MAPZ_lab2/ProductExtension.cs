﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_lab2
{
    public static class ProductExtension
    {
        public static void AddNameOrigin(this Product product)
        {
            product.Name = product.Name + "_NewOrigin_FromMalta";
        }

        public static double FindAvaragePrice<TSource>(this IEnumerable<TSource> source)
        {
            if (source is null || !source.Any())
            {
                throw new InvalidOperationException("Cannot compute average for a null or empty set.");
            }

            double aveg = 0;

            if (source is List<Product>)
            {
                List<Product> products = source as List<Product>;

                aveg = products.Average(prod => prod.Price);
            }
            
            //далі не під, бо це вже для конкретної реалізаці класу Product є
            //products.Average(predicate);

            return aveg;
        }
    }
}
