﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_lab2
{
    class ProductComparer : IComparer<Product>
    {
        public int Compare(Product p1, Product p2)
        {
            if(p1 is null || p2 is null)
            {
                throw new ArgumentNullException();
            }
            return p1.Name.Length - p2.Name.Length;
        }
    }
}
