﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_lab2
{
    public class Product
    {
        private static int idCount;

        public int Id { get; private set; }
        public string Name { get; set; }
        public double Price { get; set; }

        static Product()
        {
            idCount = 1;
        }
        public Product()
        {
            Id = idCount++;
        }
        public Product(string name, double price):this()
        {
            Name = name;
            Price = price;
        }

        public override string ToString()
        {
            return String.Format($"Id:{Id}\tName: {Name}\tPrice:{Price}$\n");
        }
    }
}
