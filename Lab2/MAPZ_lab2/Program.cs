﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MAPZ_lab2
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Product> products = ModelTest.GetProducts();

            products.Add(new Product { Name = "Apple Jam", Price = 3.99 });
            products.Add(new Product { Name = "Meatball", Price = 10.34 });

            //вивід всіх продуктів
            products.ForEach(product => Console.WriteLine(product));

            List<Storage> storages = ModelTest.GetStorages();

            storages.ForEach(storage => Console.WriteLine(storage));

            //методи розширення
            products.Where(product => product.Name.Length > 4)
                .OrderBy(prod => -prod.Price)
                .ToList()
                .ForEach(product =>
                {
                    product.AddNameOrigin();
                    Console.WriteLine(product);
                });

            Console.WriteLine("\n\n");
            //стара форма запису
            var productsList = from prod in products
                               orderby prod.Name
                               where prod.Price > 5
                               select prod;

            //анонімний клас
            var customer = new {Name= "Oleh", Age = 18, Money = 100 };
            Console.WriteLine("\nAge is : "+customer.Age+"\n");

            //IComparer
            Product[] array_prods = {products[0], products[2],products[1] };
            Array.Sort(array_prods, new ProductComparer());
            array_prods.ToList()
                       .ForEach(prod => Console.WriteLine(prod));


            Console.WriteLine("\n\n");

            //конвертацяє списків в масив і навпаки
            array_prods = products.ToArray();

            array_prods.ToList()
                       .ForEach(prod => Console.WriteLine(prod));


            //свій метод розширення, що шукає середню ціну
            Console.WriteLine("\n\n");
            double aveg = products.FindAvaragePrice();
            Console.WriteLine($"Average is : {aveg}");



            //dictionary
            Console.WriteLine("\n\n");
            var myDictionary = new Dictionary<int, Product>();
            products.ForEach(prod => myDictionary.Add(prod.Id, prod));
            
            var test = myDictionary
                .Select(one => one.Value)
                .FirstOrDefault(prod => prod.Name == "Jam");

            Console.WriteLine(test);

            //є словник через  Linq
            var mySecondDict = products.ToDictionary((k) => k.Id, (v) => v.Price);


            //групування----
            var groupByName = products
                .GroupBy(prod => prod.Name)
                .OrderBy(prod => prod.Key);

            foreach(var nameGroup in groupByName)
            {
                Console.WriteLine($"Key is {nameGroup.Key}");
                foreach (var product in nameGroup)
                {
                    Console.WriteLine($"Name: {product.Name}\tPrice: {product.Price}$\n");
                }
            }

            //є 3 джема і 2 яблука, і треба що вивесло загальну ціну джемів і
            //загальну ціну яблук
            //погрупувати в знайти суму вартості товарів в кожній групі



            var myTestGroup = products
                .GroupBy(prod => prod.Name)
                .ToDictionary(k=>k.Key, v=>v.Sum(p=>p.Price));



            //робота з storages
            Console.WriteLine("\n\n");

            var myTestick = storages
                .SelectMany(storage => storage.Products)
                .Where(prod => prod.Name.StartsWith("J"))
                .ToList();

            myTestick.ForEach(prod => Console.WriteLine(prod));


            Func<int, int, int> MyFunc = (x, y) => x + y;
        }
    }
}
