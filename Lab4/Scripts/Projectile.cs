using UnityEngine;
using System;
using System.Collections.Generic;

public class Projectile : MonoBehaviour
{
    public float damage = 20;

    public UnstaticProjectile projState;

    //speed is different for enemy
    public Vector3 direction;

    public float speed;

    public Projectile(float damag, UnstaticProjectile state)
    {
        this.damage = damag;
        this.projState = state;
    }

    //event that missile is destroyed
    public System.Action destroyed;

    public void Update()
    {
        this.transform.position += this.direction * this.speed * Time.deltaTime;
    }

    //check for collision
    private void OnTriggerEnter2D(Collider2D other)
    {
        if(this.destroyed != null)
        {
            this.destroyed.Invoke();
        }
        
        Destroy(this.gameObject);
    }
}

//projectile lightweight============


public static class ProjectileFactory
{
    private static List<UnstaticProjectile> unstaticProjectiles = new List<UnstaticProjectile>();

    public static UnstaticProjectile GetUnstaticProjectile(Vector3 direct, float speed)
    {
        var type = unstaticProjectiles.Find(project => (project.speed == speed) 
        &&(project.direction == direct));

        if(type == null)
        {
            type = new UnstaticProjectile(direct, speed);
            unstaticProjectiles.Add(type);
        }
        
        return type;
    }
}

public class UnstaticProjectile
{
    //speed is different for enemy
    public Vector3 direction;
    public float speed;

    public UnstaticProjectile(Vector3 direct, float sp)
    {
        direction = direct;
        speed = sp;
    }

}


public class ProjectileStorage
{
    private List<Projectile> projectiles = new List<Projectile>();

    public void AddProjectile(float damage, Vector3 direction, float speed)
    {
        var type = ProjectileFactory.GetUnstaticProjectile(direction, speed);
        var projectile = new Projectile(damage, type);
        projectiles.Add(projectile);
    }

    public void Render()
    {

    }
}

