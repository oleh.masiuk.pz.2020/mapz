using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


//facade pattern============
public class Facade
{
    private static Facade instance = null;

    private MainMenu mainMenu;
    private Player player;
    private Orcs orcs;

    private Facade()
    {
        mainMenu = new MainMenu();
        player = new Player();
        orcs = new Orcs();
    } 

    public static Facade Instance 
    {
        get
        {
            if(instance == null)
            {
                instance = new Facade();
            }
            return instance;
        }
    }

    public void StartNewGame()
    {
        //refresh menu
        //....
        //set new player
        //.....
        //set new enemies
        //.....
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void StartNextLevel()
    {
        //get settings for next levels
        //.....
    }
}
