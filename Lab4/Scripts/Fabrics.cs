using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Inteface==============
public interface AbstractAircraftFactory
{
    IArmor GetArmor();
    IEngine GetEngine();
    IWeapon GetWeapon();
}


//Jets Factory=====================
public class JetsFactory : AbstractAircraftFactory
{
    public IArmor GetArmor()
    {
        return new JetsArmor();
    }
    public IEngine GetEngine()
    {
        return new JetsEngine();
    }
    public IWeapon GetWeapon()
    {
        return new JetsWeapon();
    }
}


//Bombers Factory==================
public class BombersFactory : AbstractAircraftFactory
{
    public IArmor GetArmor()
    {
        return new BombersArmor();
    }
    public IEngine GetEngine()
    {
        return new BombersEngine();
    }
    public IWeapon GetWeapon()
    {
        return new BombersWeapon();
    }
}