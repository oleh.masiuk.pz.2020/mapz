using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text;
using System.IO;


public interface ILogWriter
{
    void WriteLog(string path);
}


public class LogWriter: ILogWriter
{
    public void WriteLog(string path)
    {
        string message = "It`s default LogWriter";

        using(StreamWriter writer = new StreamWriter(path, true))
        {
            writer.WriteLine(message);
            writer.WriteLine("\n\n");
        }
    }
}

public abstract class LogWriterDecorator: ILogWriter
{
    protected ILogWriter _writer;

    public LogWriterDecorator(ILogWriter otherWriter)
    {
        SetWriter(otherWriter);
    }

    public void SetWriter(ILogWriter otherWriter)
    {
        this._writer = otherWriter;
    }

    public virtual void WriteLog(string path)
    {
        if(_writer != null)
        {
            //extenshion to LogWriter function
            using(StreamWriter writer = new StreamWriter(path, true))
            {
                writer.WriteLine("LogDecoratorLine");
            }

            //LogWriter function
            _writer.WriteLog(path);

        }
        else
        {
            Debug.Log("Set writer");
        }
    }
}

public class FullLogDecorator: LogWriterDecorator
{
    public FullLogDecorator(ILogWriter writer) : base(writer)
    {
    }

    //override
    public override void WriteLog(string path)
    {
        if(_writer != null)
        {
            using(StreamWriter writer = new StreamWriter(path, true))
            {
                writer.WriteLine("FullLogDecoratorLine");

            }
            _writer.WriteLog(path);
        }
        else
        {
            Debug.Log("Set writer");
        }
    }
}

public class SimpleLogDecorator: LogWriterDecorator
{
    public SimpleLogDecorator(ILogWriter writer) : base(writer)
    {
    }

    //override
    public override void WriteLog(string path)
    {
        if(_writer != null)
        {
            using(StreamWriter writer = new StreamWriter(path, true))
            {
                writer.WriteLine("SimpleLogDecoratorLine");
            }

            _writer.WriteLog(path);

        }
        else
        {
            Debug.Log("Set writer");
        }
    }
}
