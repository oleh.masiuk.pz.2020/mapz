using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//interfaces==============
//armor
public interface IArmor
{
    float SetArmor();
}
//arrack====================
public interface IWeapon
{
    float SetWeapon();
}
//speed===========================
public interface IEngine
{
    float SetEngine();
}




//Jets====================
public class JetsArmor : IArmor
{
    public float SetArmor()
    {
        return 5;
    }
}

public class JetsWeapon : IWeapon
{
    public float SetWeapon()
    {
        return 5;
    }
}


public class JetsEngine: IEngine
{
    public float SetEngine()
    {
        return 60.0f;
    }
}



//Bombers===============================
public class BombersArmor : IArmor
{
    public float SetArmor()
    {
        return 10;
    }
}
public class BombersWeapon : IWeapon
{
    public float SetWeapon()
    {
        return 10;
    }
}
public class BombersEngine: IEngine
{
    public float SetEngine()
    {
        return 30.0f;
    }
}