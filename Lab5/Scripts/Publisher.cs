using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Globalization;
using System;
using System.Text;
using System.IO;


public interface ISubscriber
{
    void Update(string path);
}

public class LogChanger: ISubscriber
{
    public void Update(string path)
    {
        string message = "NewlogString";

        using(StreamWriter writer = new StreamWriter(path, true))
        {
            writer.WriteLine(message);
        }

    }

}

public class DateChanger:ISubscriber
{
    public void Update(string path)
    {
        DateTime localDate = DateTime.Now;

        
        string message = String.Format("{0}:{1}:{2}",localDate.Year, localDate.Month, localDate.Day);
        using(StreamWriter writer = new StreamWriter(path, true))
        {
            writer.WriteLine(message);
        }
    }
}

public class TimeChanger: ISubscriber
{
    public void Update(string path)
    {
        DateTime localDate = DateTime.Now;

        string message = String.Format("{0}:{1}:{2}",localDate.Hour, localDate.Minute, localDate.Second);

        using(StreamWriter writer = new StreamWriter(path, true))
        {
            writer.WriteLine(message);
            writer.WriteLine("\n\n");
        }

    }
}



public class Publisher
{
    List<ISubscriber> subscribers = new List<ISubscriber>();

    string state;


    public Publisher()
    {
        state = "Not changed";

        var date =new DateChanger();
        var hour = new TimeChanger();
        var log = new LogChanger();

        Subscribe(log);
        Subscribe(date);
        Subscribe(hour);

    }

    public void Subscribe(ISubscriber s)
    {
        subscribers.Add(s);
    }

    public void Unsubscribe(ISubscriber s)
    {
        subscribers.Remove(s);
    }

    public void NotifySubscriber()
    {
        string path = @"C:\Users\Acer\OneDrive\Робочий стіл\Моделювання та аналіз ПЗ\ForLab\lab5.txt";
        foreach (ISubscriber sub in subscribers)
        {
            sub.Update(path);
        }
    }

    public void StateChanged()
    {
        state = "sate has been changed";
        NotifySubscriber();
    }
}
