using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMediator
{
    void Notify(object sender, string env);
}

public class BaseComponent
{
    protected IMediator _mediator;

    public BaseComponent(IMediator mediator = null)
    {
        this._mediator = mediator;
    }

    public void SetMediator(IMediator mediator)
    {
        this._mediator = mediator;
    }
}

public class Component1 : BaseComponent
{
    public void DoA()
    {
        Debug.Log("Component 1 does A.");

        this._mediator.Notify(this, "A");
    }

    public void DoB()
    {
        Debug.Log("Component 1 does B.");

        this._mediator.Notify(this, "B");
    }
}

public class Component2 : BaseComponent
{
    public void DoC()
    {
        Debug.Log("Component 2 does C.");

        this._mediator.Notify(this, "C");
    }

    public void DoD()
    {
        Debug.Log("Component 2 does D.");

        this._mediator.Notify(this, "D");
    }
}


public class ConcreteMediator: IMediator
{
    private Component1 _component1;

    private Component2 _component2;

    public ConcreteMediator(Component1 component1, Component2 component2)
    {
        this._component1 = component1;
        this._component1.SetMediator(this);
        this._component2 = component2;
        this._component2.SetMediator(this);
    } 

    public void Notify(object sender, string ev)
    {
        if (ev == "A")
        {
            Debug.Log("Mediator reacts on A and triggers folowing operations:");
            this._component2.DoC();
        }
        if (ev == "D")
        {
            Debug.Log("Mediator reacts on D and triggers following operations:");
            this._component1.DoB();
            this._component2.DoC();
        }
    }
}
