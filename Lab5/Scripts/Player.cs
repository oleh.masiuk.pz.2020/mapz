using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    
    //instance of object for singleton
    public static Player Instance {get; private set;}

    public float speed = 15.0f;
    //speed is less if move 2 sides
    public float spReduction = 1.3f;

    //private bool _missileActive;

    //for many missilse
    public int maxMissiles = 3;

    public int missileCount = 0;

    public Projectile missilePrefab;

    //make sure that there is only ever one instance
    //Then, if it doesn’t match, the script knows that it’s a 
    //duplicate and it can delete itself.
    private void Awake()
    {
        if(Instance !=null && Instance !=this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }

    private void Update()
    {
        //if 2 keys pressed
        if((Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))  
        && (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)))
        {
            this.transform.position += Vector3.up * this.speed / spReduction * Time.deltaTime;
            this.transform.position += Vector3.left * this.speed / spReduction * Time.deltaTime;
        }
        else if((Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))  
        && (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)))
        {
            this.transform.position += Vector3.up * this.speed / spReduction * Time.deltaTime;
            this.transform.position += Vector3.right * this.speed / spReduction * Time.deltaTime;
        }
        else if((Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))  
        && (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)))
        {
            this.transform.position += Vector3.down * this.speed / spReduction * Time.deltaTime;
            this.transform.position += Vector3.right * this.speed / spReduction * Time.deltaTime;
        }
        else if((Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))  
        && (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)))
        {
            this.transform.position += Vector3.down * this.speed / spReduction * Time.deltaTime;
            this.transform.position += Vector3.left * this.speed / spReduction * Time.deltaTime;
        }

        //if we press key W or up_arrow - move
        else if(Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
        {
            this.transform.position += Vector3.up * this.speed * Time.deltaTime;
        }
        else if(Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
        {
            this.transform.position += Vector3.down * this.speed * Time.deltaTime;
        }
        else if(Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            this.transform.position += Vector3.left * this.speed * Time.deltaTime;
        }
        else if(Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            this.transform.position += Vector3.right * this.speed * Time.deltaTime;
        }

        //if player shoot,  0 -left mouse click
        if(Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
        {
            Shoot();
        }
    }
    private void Shoot()
    {
        //shoot only 1 missile in a time
        if(missileCount < maxMissiles)
        {
            missileCount++;
            Projectile projectile = Instantiate(this.missilePrefab, this.transform.position, Quaternion.identity);
            //subscribe to event
            projectile.destroyed += MissileDestroyed;

            //_missileActive = true;
            //print(missileCount);
            //print(maxMissiles);
        }
    }
    private void MissileDestroyed()
    {
        //_missileActive = false;
        missileCount--;
    }

    //lose condition
    private void OnTriggerEnter2D(Collider2D other)
    {
        //game over
        if(other.gameObject.layer == LayerMask.NameToLayer("Invader") ||
        other.gameObject.layer == LayerMask.NameToLayer("Orc_Missile"))
        {
            //restart the game
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}
